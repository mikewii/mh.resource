#include "fileiterator.hpp"
#include <iostream>
#include <fstream>
#include <filesystem>
#include <fmt/format.h>

#include <libmh/tools/arc/common.hpp>
#include <libmh/tools/istream.hpp>

auto iostreamfun(const mh::tools::arc::FileHeader&) noexcept(false) -> std::unique_ptr<std::iostream>
{
    return std::make_unique<std::stringstream>();
}

auto FileIterator::run(void) -> bool
{
    auto perms = std::filesystem::directory_options::skip_permission_denied;

    try {
        std::filesystem::recursive_directory_iterator it(m_inpath, perms);

        for (const auto& entry : it) {
            if (!entry.is_regular_file())
                continue;

            if (!isARC(entry.path()))
                continue;

            if (!uncompressAll(entry))
                throw std::runtime_error("Failed to gather info");
        }

    } catch (const std::exception& e) {
        std::cerr << e.what()
                  << std::endl;

        return false;
    }

    return true;
}

auto FileIterator::setInputPath(const std::filesystem::path& path) -> void { m_inpath = path; }
auto FileIterator::setOutputPath(const std::filesystem::path& path) -> void { m_outpath = path; }

auto FileIterator::dump(void) -> bool
{
    if (m_info.empty())
        return false;

    std::string start   = "std::vector<Guess> out = {\n";
    std::string end     = "};";
    std::ofstream out{m_outpath};
    auto count = 0;

    if (!out.is_open())
        return false;

    out << start;

    for (auto it = m_info.begin(); it != m_info.end(); it++) {
        std::string line;

        if (count == 0)
            line += "      {";
        else
            line += "    , {";

        line += fmt::format("0x{0:x}", it->first);
        line += ", {{";

        for (const auto& value : it->second)
            line += fmt::format(  "0x{0:x}, 0x{1:x}"
                                , value.magic
                                , value.version);

        line += "}}}\n";

        out << line;

        ++count;
    }

    out << end;

    return true;
}

auto FileIterator::isARC(const std::filesystem::path& path) -> bool
{
    if (std::filesystem::file_size(path) <= sizeof(mh::tools::arc::Header))
        return false;

    mh::tools::arc::Header header;
    std::ifstream in(path, std::ios::in | std::ios::binary);

    if (!in.is_open())
        return false;

    mh::tools::istream::read(in, &header, sizeof(header));

    if (header.isARC() || header.isCRA())
        return true;

    return false;
}

auto FileIterator::gatherInfo(mh::tools::arc::Reader& reader,
                              mh::tools::arc::Reader::streamVector& vector) -> bool
{
    for (const auto& data : vector) {
        MagicVersion mv;

        data.first->seekg(std::ios::beg, std::ios::beg);

        mh::tools::istream::read(  *data.first
                                 , &mv.magic
                                 , sizeof(mv.magic));

        mh::tools::istream::read(  *data.first
                                 , &mv.version
                                 , sizeof(mv.version));

        m_info[data.second.ResourceHash].insert(mv);

        if (   mv.magic == mh::tools::arc::Header::ARC_MAGIC
            || mv.magic == mh::tools::arc::Header::CRA_MAGIC) {
            auto stream = std::make_unique<std::stringstream>();

            reader.uncompress(  data.second
                              , *stream);

            if (!uncompressAll(std::move(stream)))
                return false;
        }
    }

    return true;
}

auto FileIterator::uncompressAll(const std::filesystem::path& path) -> bool
{
    mh::tools::arc::Reader reader(path);
    mh::tools::arc::Reader::streamVector streams;

    reader.setUncompressedSizeLimit(8);

    if (!reader.read())
        return false;

    if (!reader.uncompress_all_async(  path
                                     , iostreamfun
                                     , streams))
        return false;

    return gatherInfo(reader, streams);
}

auto FileIterator::uncompressAll(std::unique_ptr<std::iostream>&& iostream) -> bool
{
    mh::tools::arc::Reader reader(std::move(iostream));
    mh::tools::arc::Reader::streamVector streams;

    reader.setUncompressedSizeLimit(8);

    if (!reader.read())
        return false;

    if (!reader.uncompress_all(  iostreamfun
                               , streams))
        return false;

    return gatherInfo(reader, streams);
}
