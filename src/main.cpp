#include <cxxopts.hpp>
#include <iostream>
#include <filesystem>
#include "fileiterator.hpp"

auto checkInputPath(const std::filesystem::path& path) noexcept -> bool
{
    try {
        if (!std::filesystem::exists(path))
            throw std::runtime_error("Input path doesnt exist");

        if (!std::filesystem::is_directory(path))
            throw std::runtime_error("Input path must be directory");

    } catch (const std::exception& e) {
        std::cerr << e.what()
                  << std::endl;

        return false;
    }

    return true;
}

auto checkOutputPath(const std::filesystem::path& path,
                     std::filesystem::path& out) noexcept -> bool
{
    std::filesystem::path fspath{path};
    std::filesystem::path filename = fspath.filename();
    auto count = 1;

    try {
        while (true) {
            if (std::filesystem::exists(fspath))
                fspath.replace_filename(  filename.string()
                                        + '.'
                                        + std::to_string(count));
            else
                break;

            ++count;
        }
    } catch (const std::exception& e) {
        std::cerr << e.what()
                  << std::endl;

        return false;
    }

    out = fspath;

    return true;
}

auto main(int argc, char* argv[]) -> int
{
    cxxopts::Options options("mh.resource", "Recursively obtain resource magic+version from mh ROM");
    std::filesystem::path input, output;
    std::filesystem::path fsoutput;
    FileIterator fit;

    options.add_options()
        ("i,input", "Input dir", cxxopts::value<std::filesystem::path>(input))
        ("o,output", "Output file", cxxopts::value<std::filesystem::path>(output))
        ("h,help", "Print help and exit.");

    try {
        auto result = options.parse(argc, argv);

        if (input.empty())
            throw std::runtime_error("Empty input");

        if (output.empty())
            throw std::runtime_error("Empty output");

        if (result.count("help")) {
            std::cout << options.help() << std::endl;
            std::exit(0);
        }

        if (!checkInputPath(input))
            return 1;

        if (!checkOutputPath(output, fsoutput))
            return 1;

        fit.setInputPath(input);
        fit.setOutputPath(fsoutput);

        if (!fit.run())
            throw std::runtime_error("Failed to iterate through files");

        if (!fit.dump())
            throw std::runtime_error("Failed to dump");

    } catch (const std::exception& e) {
        std::cerr << "Error parsing options: "
                  << e.what()
                  << std::endl;
        return 1;
    }

    return 0;
}
