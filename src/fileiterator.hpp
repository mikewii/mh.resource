#pragma once
#include <libmh/types.hpp>
#include <libmh/tools/arc/reader.hpp>
#include <filesystem>
#include <set>
#include <map>

struct MagicVersion {
    u32 magic;
    u32 version;

    auto operator < (const MagicVersion& other) const noexcept -> bool
    {
        if (   other.magic < magic
            && other.version < version)
            return true;

        return false;
    }
};

class FileIterator
{
private:
    std::filesystem::path m_inpath;
    std::filesystem::path m_outpath;
    std::map<u32, std::set<MagicVersion>> m_info;

public:
    auto run(void) -> bool;

    auto setInputPath(const std::filesystem::path& path) -> void;
    auto setOutputPath(const std::filesystem::path& path) -> void;

    auto dump(void) -> bool;

private:
    auto isARC(const std::filesystem::path& path) -> bool;

    auto gatherInfo(mh::tools::arc::Reader& reader,
                    mh::tools::arc::Reader::streamVector& vector) -> bool;

    auto uncompressAll(const std::filesystem::path& path) -> bool;
    auto uncompressAll(std::unique_ptr<std::iostream>&& iostream) -> bool;
};

struct Guess {
    u32 ResourceHash;

    std::vector<std::pair<u32,u32>> MagicVersion;
};
