cmake_minimum_required(VERSION 3.14)

include(cmake/helpers.cmake)

project(mh.resource VERSION 1.0.0.0 LANGUAGES C CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
option(ENABLE_GUEST_BUILD "" ON)

file(GLOB_RECURSE PROJECT_SOURCES CONFIGURE_DEPENDS RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}"
    "${CMAKE_CURRENT_SOURCE_DIR}/src/*.cpp")
file(GLOB_RECURSE PROJECT_HEADERS CONFIGURE_DEPENDS RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}"
    "${CMAKE_CURRENT_SOURCE_DIR}/src/*.hpp")
set(PROJECT_FILES ${PROJECT_SOURCES} ${PROJECT_HEADERS})

if (NOT TARGET mh)
add_subdirectory(extern/libmh)
endif()

if (NOT TARGET fmt)
add_subdirectory(extern/fmt)
endif()

if (NOT TARGET cxxopts)
add_subdirectory(extern/cxxopts)
endif()

add_executable(${PROJECT_NAME} ${PROJECT_FILES})

target_include_directories(${PROJECT_NAME} PRIVATE src)

if (ENABLE_GUEST_BUILD)
target_link_libraries(${PROJECT_NAME} PUBLIC -static -static-libgcc -static-libstdc++)
endif()

target_link_libraries(${PROJECT_NAME} PUBLIC mh::mh)
target_link_libraries(${PROJECT_NAME} PUBLIC fmt::fmt)
target_link_libraries(${PROJECT_NAME} PUBLIC cxxopts::cxxopts)

create_target_directory_groups(${PROJECT_NAME})
